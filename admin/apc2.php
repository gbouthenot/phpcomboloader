<!DOCTYPE html>
<html>
<head>
<title>YUI Combo cache summary</title>
<style>
table.ymain {
    border:1px solid #000;
    border-collapse:collapse;
}
table.ymain td {
    border:1px solid #ddd;
}

</style>
</head>
<body>


<pre>
<?php

ini_set("display_warnings", true);
ini_set("display_errors", true);
error_reporting(E_ALL);

include("../ini/conf.php");

$aCacheInfo = apc_cache_info("user");
print_r($aCacheInfo);
?>
</pre>

<table class="ymain">

    <?php
    $formatDateFunc = function($timestamp) {
        return date("Y-m-d H:i:s", $timestamp);
    };
    
    $apcIterator = new APCIterator('user', '/^' . APC_PREFIX . '/');
    foreach( $apcIterator as $aCache) {
        $key          = $aCache["key"];
        $sizeReported = $aCache["mem_size"];
        $value        = $aCache["value"];
        $aFiles       = $value[2];
        $numHits      = $aCache["num_hits"];
        $refCount     = $aCache["ref_count"];
        $timeCreate   = $aCache["creation_time"];
        $timeLast     = $aCache["access_time"];
        $lenGunzip    = $value[3];
        $lenGzip      = $value[4];
        
        ?>
        <tr>
            <td><?= $key ?></td>
            <td><?= $lenGunzip ?></td>
            <td><?= $lenGzip   ?></td>
            <td><?= $numHits ?> / <?= $refCount ?></td>
            <td><?= $formatDateFunc($timeCreate) ?></td>
            <td><?= $formatDateFunc($timeLast) ?></td>
            <td><?= implode("<br />", $aFiles) ?></td>
        </tr>
        <?php
    }
    ?>

</table>
</body>
</html>